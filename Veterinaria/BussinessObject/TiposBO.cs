﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessObject;
using System.Data;
using Entity;

namespace BussinessObject
{
    public class TiposBO
    {
        TiposDAO tdao = new TiposDAO();

        public DataTable getTiposDataTable()
        {
            return tdao.getTipoDataTable();
        }
        public LinkedList<Tipo> getLinkedTipo()
        {
            return tdao.getTipoLink();
        }

       

        public void create(string codigo, string tipo)
        {
            Tipo newTipo = new Tipo(codigo, tipo);
            tdao.addTipo(newTipo);
        }


        public void update(string codigo, string tipo, string oldDni)
        {
            Tipo newTipo = new Tipo( codigo,  tipo);
            tdao.updateTipo(newTipo, oldDni);
        }

        
        /*
         *Delete a user by ID
         */
        public void delete(string nombre)
        {
            tdao.deleteTipo (nombre);
        }

    }
}

