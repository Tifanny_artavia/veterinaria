﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessObject;
using System.Data;
using Entity;


namespace BussinessObject
{
    public class RazasBO
    {
        RazasDAO rdao = new RazasDAO();

        public DataTable getRazaDataTable()
        {
            return rdao.getRazaDataTable();
        }
        public LinkedList<Razas> getLinkedRazas()
        {
            return rdao.getRazaLink();
        }
        public void create(string codigo, string raza)
        {
            Razas newraza = new Razas(codigo, raza);
            rdao.addRaza(newraza);
        }


        public void update(string codigo, string raza, string oldDni)
        {
            Razas newRaza = new Razas(codigo, raza);
            rdao.updateRaza(newRaza, oldDni);
        }
        /*
        *Delete a user by ID
        */
        public void delete(string nombre)
        {
            rdao.deleteRaza(nombre);
        }
    }
}
