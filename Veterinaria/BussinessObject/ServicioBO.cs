﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessObject;
using System.Data;
using Entity;

namespace BussinessObject
{
    public class ServicioBO
    {
        ServiciosDAO sdao = new ServiciosDAO();
        public DataTable getServDataTable()
        {
            return sdao.getServDataTable();
        }
        public LinkedList<Servicio> getLinkedServ()
        {
            return sdao.getServLink();
        }

      

        public void create(string codigo, string nombre, string descripcion, int precio)
        {
            Servicio newServ = new Servicio(codigo,nombre,descripcion,precio);
            sdao.addServ(newServ);
        }


        public void update(string codigo, string nombre, string descripcion, int precio, int Old)
        {
            Servicio newServ = new Servicio(codigo, nombre, descripcion, precio);
            sdao.updateServ(newServ, Old);
        }

        
        public void delete(int precio)
        {
            sdao.deleteServ(precio);
        }

    }
}
