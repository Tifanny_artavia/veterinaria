﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessObject;
using Entity;
using System.Data;

namespace BussinessObject
{
    public class TratamientoBO
    { 
        TratamientoDAO tdao = new TratamientoDAO();
        public DataTable getTraDataTable()
        {
            return tdao.getTratDataTable();
        }
        public LinkedList<Tratamiento> getLinkedTrat()
        {
            return tdao.getTratLink();
        }



        public void create(string codigo, string nombre, string descripcion, int precio)
        {
            Tratamiento newTrat = new Tratamiento(codigo, nombre, descripcion, precio);
            tdao.addTrat(newTrat);
        }


        public void update(string codigo, string nombre, string descripcion, int precio, int Old)
        {
            Tratamiento newTrat = new Tratamiento(codigo, nombre, descripcion, precio);
            tdao.updateTrat(newTrat, Old);
        }


        public void delete(int precio)
        {
            tdao.deleteTrat(precio);
        }
    }
}
