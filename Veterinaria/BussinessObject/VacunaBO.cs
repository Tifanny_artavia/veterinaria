﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessObject;
using Entity;
using System.Data;
using ComposicionAgregacion;

namespace BussinessObject
{
    public class VacunaBO
    {
        VacunaDAO vdao = new VacunaDAO();
        CVacuna vacuna = new CVacuna();
        public DataTable getVacunaDataTable()
        {
            return vdao.getVacunaDataTable();
        }
        public LinkedList<Vacuna> getLinkedVacuna()
        {
            return vdao.getVacLink();
        }



        public void create(string codigo, string nombre, string descripcion, int precio)
        {
            Vacuna newVacuna = new Vacuna(codigo, nombre, descripcion, precio);
            vdao.addVacuna(newVacuna);
            
        }


        public void update(string codigo, string nombre, string descripcion, int precio, int Old)
        {
            Vacuna newVac = new Vacuna(codigo, nombre, descripcion, precio);
            vdao.updateVacuna(newVac, Old);
        }


        public void delete(int precio)
        {
            vdao.deleteVacuna(precio);
        }


    }
}
