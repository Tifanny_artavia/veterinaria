﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessObject;
using System.Data;
using Entity;


namespace BussinessObject
{
    public class UsuarioMascotaBO
    {
        UsuarioDAO udao = new UsuarioDAO();
        MascotaDAO mdao = new MascotaDAO();

        public Usuario getLogin(int cedula , string contra)
        {
            Usuario u = null;
            foreach (Usuario item in udao.getUserLink())
            {
                if ((cedula == item.cedula) && contra.Equals(item.contra))
                {
                    u = item;
                }
            }
            return u;
        }
        public DataTable getUsersDataTable()
        {
            return udao.getUserDataTable();
        }
        public LinkedList<Usuario> getLinkedUser()
        {
            return udao.getUserLink();
        }

        public LinkedList<Usuario> getLinkedClients()
        {
            return udao.getClientsLink();
        }

        public DataTable getClientsDataTable()
        {
            return udao.getClientDataTable();
        }

        
        public void create(int cedula, string nombre, string apellido, string telefono, string genero, string correo, string estado, string type, string contra)
        {
            Usuario newUser = new Usuario(cedula, nombre, apellido, telefono, genero, correo, estado, type, contra);
            udao.addUser(newUser);
        }
       

        public void update(int cedula, string nombre, string apellido, string telefono, string genero, string correo, string estado, string type, string contra, int oldDni)
        {
            Usuario newUser = new Usuario(cedula, nombre, apellido, telefono, genero, correo, estado, type, contra);
            udao.updateUser(newUser, oldDni);
        }

        /*
         *Delete a user by ID
         */
        public void delete(int ced)
        {
            udao.deleteUser(ced);
        }
//--------------------------------------------------------------------------------------------------------------------------------------------------------
        
        public DataTable getMascoDataTable()
        {
            return mdao.getMascoDataTable();
        }
        public LinkedList<Mascota> getMasoLink()
        {
            return mdao.getMasoLink();
        }
        //[nombre],[fecha_ingreso],[fecha_nacimiento],[genero],[type],[raza],[color],[peso],[ced_duenno],[nom_duenno],[ape_duenno],[direccion],[telefono],[email],[observaciones],[estado]
        public void create(string nombre_mascota, DateTime fecha_ingreso, DateTime fecha_nacimiento, string sexo, string tipo_mascota, string raza, string color, string peso, string dirección, string telefono, string email, string observaciones, string estado)
        {
            Mascota newMasc = new Mascota(nombre_mascota, fecha_ingreso, fecha_nacimiento, sexo, tipo_mascota, raza, color, peso, dirección, telefono, email, observaciones, estado);
            mdao.addMasco(newMasc);
        }

        public void update(string nombre_mascota, DateTime fecha_ingreso, DateTime fecha_nacimiento, string sexo, string tipo_mascota, string raza, string color, string peso, string dirección, string telefono, string email, string observaciones, string estado, int oldId)
        {
            Mascota newMasco = new Mascota(nombre_mascota, fecha_ingreso, fecha_nacimiento, sexo, tipo_mascota, raza, color, peso, dirección, telefono, email, observaciones, estado);
            mdao.updateMasco(newMasco, oldId);
        }

        /*
         *Delete a user by ID
         */
        public void deleteM(int Id)
        {
            mdao.deleteMascota(Id);
        } 
    }
}
