﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessObject;
using System.Data;
using Entity;

namespace BussinessObject
{
    public class MascotaBO
    {
        MascotaDAO mdao = new MascotaDAO();
        public DataTable getMascoDataTable()
        {
            return mdao.getMascoDataTable();
        }
        public LinkedList<Mascota> getMasoLink()
        {
            return mdao.getMasoLink();
        }
        //[nombre],[fecha_ingreso],[fecha_nacimiento],[genero],[type],[raza],[color],[peso],[ced_duenno],[nom_duenno],[ape_duenno],[direccion],[telefono],[email],[observaciones],[estado]
        public void createMascota(string nombre_mascota, DateTime fecha_ingreso, DateTime fecha_nacimiento, string sexo, string tipo_mascota, string raza, string color, string peso,string direccion, string telefono, string email, string observaciones, string estado)
        {
            Mascota newMasc = new Mascota(nombre_mascota,  fecha_ingreso,  fecha_nacimiento,  sexo,  tipo_mascota,  raza,  color,  peso,  direccion,  telefono,  email,  observaciones,  estado);
            mdao.addMasco(newMasc);
        }

        public void updateMascota(string nombre_mascota, DateTime fecha_ingreso, DateTime fecha_nacimiento, string sexo, string tipo_mascota, string raza, string color, string peso, string direccion, string telefono, string email, string observaciones, string estado, int oldId)
        {
            Mascota newMasco = new Mascota( nombre_mascota,  fecha_ingreso,  fecha_nacimiento,  sexo,  tipo_mascota,  raza,  color,  peso,  direccion,  telefono,  email,  observaciones,  estado);
            mdao.updateMasco(newMasco, oldId);
        }

        /*
         *Delete a user by ID
         */
        public void delete(int Id)
        {
            mdao.deleteMascota(Id);
        }
    }
}
