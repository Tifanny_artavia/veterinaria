﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ComposicionAgregacion
{
    public class CVacuna
    {
        public string _nombre;
        public int _cantidad;

        public CVacuna(string nombre, int cantidad)
        {
            _nombre = nombre;
            _cantidad = cantidad;
        }

        public CVacuna()
        {
        }

        public string nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }
        public int cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; }
        }

        public void vacuna(string nombre, int cantidad)
        {
             nombre = "";
             cantidad = 0;

        }

    }
}
