﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Componente
{
    public partial class CSala : UserControl
    {
        public CSala()
        {
            InitializeComponent();
        }

        private void Ocupado (Button boton)
        {
            boton.Text = "En Cirugía";
            boton.BackColor = Color.Red;
            boton.Enabled = false;
        }

        private void btnSala1_Click(object sender, EventArgs e)
        {
            Ocupado(this.btnSala1);
        }

        private void btnSala2_Click(object sender, EventArgs e)
        {
            Ocupado(this.btnSala2);

        }

        private void btnSala3_Click(object sender, EventArgs e)
        {
            Ocupado(this.btnSala3);

        }
    }
}
