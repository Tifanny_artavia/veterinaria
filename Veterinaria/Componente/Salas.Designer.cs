﻿
namespace Componente
{
    partial class CSala
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSala1 = new System.Windows.Forms.Button();
            this.btnSala2 = new System.Windows.Forms.Button();
            this.btnSala3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSala1
            // 
            this.btnSala1.Location = new System.Drawing.Point(34, 115);
            this.btnSala1.Name = "btnSala1";
            this.btnSala1.Size = new System.Drawing.Size(75, 181);
            this.btnSala1.TabIndex = 0;
            this.btnSala1.Text = "Sala 1";
            this.btnSala1.UseVisualStyleBackColor = true;
            this.btnSala1.Click += new System.EventHandler(this.btnSala1_Click);
            // 
            // btnSala2
            // 
            this.btnSala2.Location = new System.Drawing.Point(249, 115);
            this.btnSala2.Name = "btnSala2";
            this.btnSala2.Size = new System.Drawing.Size(75, 181);
            this.btnSala2.TabIndex = 1;
            this.btnSala2.Text = "Sala 2";
            this.btnSala2.UseVisualStyleBackColor = true;
            this.btnSala2.Click += new System.EventHandler(this.btnSala2_Click);
            // 
            // btnSala3
            // 
            this.btnSala3.Location = new System.Drawing.Point(454, 115);
            this.btnSala3.Name = "btnSala3";
            this.btnSala3.Size = new System.Drawing.Size(75, 181);
            this.btnSala3.TabIndex = 2;
            this.btnSala3.Text = "Sala 3";
            this.btnSala3.UseVisualStyleBackColor = true;
            this.btnSala3.Click += new System.EventHandler(this.btnSala3_Click);
            // 
            // CSala
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Componente.Properties.Resources.sala;
            this.Controls.Add(this.btnSala3);
            this.Controls.Add(this.btnSala2);
            this.Controls.Add(this.btnSala1);
            this.Name = "CSala";
            this.Size = new System.Drawing.Size(596, 328);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSala1;
        private System.Windows.Forms.Button btnSala2;
        private System.Windows.Forms.Button btnSala3;
    }
}
