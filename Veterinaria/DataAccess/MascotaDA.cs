﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class MascotaDA
    {
        //DataBase connection
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionDB"].ConnectionString);

        public DataTable selectMascota()
        {
            SqlCommand command = new SqlCommand("SELECT [id],[nombre],[fecha_ingreso],[fecha_nacimiento],[genero],[type],[raza],[color],[peso],[direccion],[telefono],[email],[observaciones],[estado] FROM [Veterinaria].[UTN].[mascotas]", connection);
            command.CommandType = CommandType.Text;
            SqlDataAdapter data = new SqlDataAdapter(command);
            DataTable masc = new DataTable();
            data.Fill(masc);

            return masc;
        }
        public void conMasc(string SQL)
        {
            SqlCommand command = new SqlCommand(SQL, connection);
            try
            {
                Console.WriteLine(SQL);
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
