﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessObject;
using Entity;
using System.Data;
using DataAccess;

namespace DataAccessObject
{
    public class ServiciosDAO
    {
        ServiciosDA sda = new ServiciosDA();

        public LinkedList<Servicio> getServLink()
        {
            LinkedList<Servicio> srv = new LinkedList<Servicio>();
            DataTable dataServ = sda.selectServ();
            DataTableReader reader = dataServ.CreateDataReader();
            do
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Servicio serv = new Servicio(int.Parse(reader[0].ToString()), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), int.Parse(reader[4].ToString()));
                        srv.AddLast(serv);
                    }
                }
            } while (reader.NextResult());
            return srv;
        }


        public DataTable getServDataTable()
        {
            return sda.selectServ();
        }
        public LinkedList<Servicio> getServiLink()
        {
            LinkedList<Servicio> serv = new LinkedList<Servicio>();
            DataTable dataServ = sda.selectServ();
            DataTableReader reader = dataServ.CreateDataReader();
            do
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Servicio srv = new Servicio(int.Parse(reader[0].ToString()), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), int.Parse(reader[4].ToString()));
                        serv.AddLast(srv);
                    }
                }
            } while (reader.NextResult());
            return serv;
        }



        public void addServ(Servicio s)
        {
            string value = "'" + s.codigo + "','" + s.nombre + "','" + s.descipcion + "','" + s.precio + "'";
            string SQL = "INSERT INTO UTN.servicios_admin (codigo, nombre, descripcion, precio) VALUES (" + value + ")";
            sda.conServ(SQL);
        }


        public void deleteServ(int precio)
        {
            string SQL = "DELETE FROM UTN.servicios_admin WHERE precio = '" + precio + "'";
            sda.conServ(SQL);
        }


        public void updateServ(Servicio s, int precio)
        {
            string SQL = "UPDATE UTN.servicios_admin SET codigo = '" + s.codigo + "', nombre = '" + s.nombre + "', descripcion = '" + s.descipcion + "', precio = '" + s.precio + "' WHERE precio = '" + precio + "'";
            sda.conServ(SQL);
        }

    }
}
