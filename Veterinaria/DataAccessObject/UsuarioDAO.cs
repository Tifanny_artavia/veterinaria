﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessObject;
using Entity;
using System.Data;
using DataAccess;


namespace DataAccessObject
{
    public class UsuarioDAO
    {
        UsuarioDA uda = new UsuarioDA();

        public LinkedList<Usuario> getUserLink()
        {
            LinkedList<Usuario> users = new LinkedList<Usuario>();
            DataTable dataUsers = uda.selectUsers();
            DataTableReader reader = dataUsers.CreateDataReader();
            do
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Usuario user = new Usuario(int.Parse(reader[0].ToString()), int.Parse(reader[1].ToString()), reader[2].ToString(), reader[3].ToString(), reader[4].ToString(), reader[5].ToString(), reader[6].ToString(), reader[7].ToString(), reader[8].ToString(), reader[9].ToString());
                        users.AddLast(user);
                    }
                }
            } while (reader.NextResult());
            return users;
        }
       
       
        public DataTable getUserDataTable()
        {
            return uda.selectUsers();
        }
        public LinkedList<Usuario> getClientsLink()
        {
            LinkedList<Usuario> users = new LinkedList<Usuario>();
            DataTable dataUsers = uda.selectCliente();
            DataTableReader reader = dataUsers.CreateDataReader();
            do
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Usuario user = new Usuario(int.Parse(reader[0].ToString()), int.Parse(reader[1].ToString()), reader[2].ToString(), reader[3].ToString(), reader[4].ToString(), reader[5].ToString(), reader[6].ToString(), reader[7].ToString(), reader[8].ToString(), reader[9].ToString());
                        users.AddLast(user);
                    }
                }
            } while (reader.NextResult());
            return users;
        }

        public DataTable getClientDataTable()
        {
            return uda.selectCliente();
        }

        public void addUser(Usuario u)
        {
            string value = "'" + u.cedula + "','" + u.nombre + "','" + u.apellido + "','" + u.telefono + "','" + u.genero + "','" + u.correo + "','" + u.estado + "','" + u.type + "','" + u.contra + "'";
            string SQL = "INSERT INTO UTN.users (cedula, nombre, apellido, telefono, genero,correo,estado,type,password) VALUES (" + value + ")";
            uda.conUser(SQL);
        }
        public void addCliente(Usuario u)
        {
            string value = "'" + u.cedula + "','" + u.nombre + "','" + u.apellido + "','" + u.telefono + "','" + u.genero + "','" + u.correo + "','" + u.estado + "','" + u.type + "','" + u.contra + "'";
            string SQL = "INSERT INTO UTN.users (cedula, nombre, apellido, telefono, genero,correo,estado,type,password) VALUES (" + value + ")";
            uda.conUser(SQL);
        }

        public void deleteUser(int cedula)
        {
            string SQL = "DELETE FROM UTN.users WHERE cedula = '" + cedula + "'";
            uda.conUser(SQL);
        }


        public void updateUser(Usuario u, int cedula)
        {
            string SQL = "UPDATE UTN.users SET cedula = '" + u.cedula + "', nombre = '" + u.nombre + "', apellido = '" + u.apellido + "', telefono = '" + u.telefono + "', genero = '" + u.genero + "',correo = '" + u.correo + "' ,estado = '" + u.estado + "' ,type = '"+ u.type + "',password = '" + u.contra + "' WHERE cedula = '" + cedula + "'";
            uda.conUser(SQL);
        }

    }
}
