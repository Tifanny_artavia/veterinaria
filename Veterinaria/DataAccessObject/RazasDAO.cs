﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessObject;
using Entity;
using System.Data;
using DataAccess;

namespace DataAccessObject
{
    public class RazasDAO
    {
        RazasDA vda = new RazasDA();

        public LinkedList<Razas> getRazaLink()
        {
            LinkedList<Razas> vc = new LinkedList<Razas>();
            DataTable dataRaza = vda.selectRaza();
            DataTableReader reader = dataRaza.CreateDataReader();
            do
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Razas vac = new Razas(int.Parse(reader[0].ToString()), reader[1].ToString(), reader[2].ToString());
                        vc.AddLast(vac);
                    }
                }
            } while (reader.NextResult());
            return vc;
        }


        public DataTable getRazaDataTable()
        {
            return vda.selectRaza();
        }
        public LinkedList<Razas> getRazLink()
        {
            LinkedList<Razas> v = new LinkedList<Razas>();
            DataTable dataRaza = vda.selectRaza();
            DataTableReader reader = dataRaza.CreateDataReader();
            do
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Razas vac = new Razas(int.Parse(reader[0].ToString()), reader[1].ToString(), reader[2].ToString());
                        v.AddLast(vac);
                    }
                }
            } while (reader.NextResult());
            return v;
        }



        public void addRaza(Razas r)
        {
            string value = "'" + r.codigo + "','" + r.raza + "'";
            string SQL = "INSERT INTO UTN.raza (codigo, nombre) VALUES (" + value + ")";
            vda.conRaza(SQL);
        }


        public void deleteRaza(string nombre)
        {
            string SQL = "DELETE FROM UTN.raza WHERE nombre = '" + nombre + "'";
            vda.conRaza(SQL);
        }


        public void updateRaza(Razas t, string nombre)
        {
            string SQL = "UPDATE UTN.raza SET codigo = '" + t.codigo + "', raza = '" + t.raza + "'";
            vda.conRaza(SQL);
        }

    }
}
