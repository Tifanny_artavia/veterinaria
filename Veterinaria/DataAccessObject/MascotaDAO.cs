﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessObject;
using Entity;
using System.Data;
using DataAccess;


namespace DataAccessObject
{
    public class MascotaDAO
    {
        MascotaDA mda = new MascotaDA();

        public LinkedList<Mascota> getMasoLink()
        {
            LinkedList<Mascota> masc = new LinkedList<Mascota>();
            DataTable dataMasc = mda.selectMascota();
            DataTableReader reader = dataMasc.CreateDataReader();
            do
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Mascota mascota = new Mascota(int.Parse(reader[0].ToString()), reader[1].ToString(), DateTime.Parse(reader[2].ToString()), DateTime.Parse(reader[3].ToString()), reader[4].ToString(), reader[5].ToString(), reader[6].ToString(), reader[7].ToString(), reader[8].ToString(), reader[9].ToString(), reader[10].ToString(), reader[11].ToString(), reader[12].ToString(), reader[13].ToString());
                        masc.AddLast(mascota);
                    }
                    //int id, string nombre_mascota, DateTime fecha_ingreso, DateTime fecha_nacimiento, string sexo, string tipo_mascota, string raza, string color, string peso, int cedula, string nombre, string apellido, string dirección, string telefono, string email, string observaciones, string estado
                }
            } while (reader.NextResult());
            return masc;
        }



        public DataTable getMascoDataTable()
        {
            return mda.selectMascota();
        }
        public void addMasco(Mascota u)
        {
            string value = "'" + u.nombre_mascota + "','" + u.fecha_ingreso + "','" + u.fecha_nacimiento + "','" + u.sexo + "','" + u.tipo_mascota + "','" + u.raza + "','" + u.color + "','" + u.peso  + "','" + u.direccion + "','" + u.telefono + "','" + u.email + "','" + u.observaciones + "','" + u.estado + "'";
            string SQL = "INSERT INTO UTN.mascotas(nombre,fecha_ingreso,fecha_nacimiento,genero,type,raza,color,peso,direccion,telefono,email,observaciones,estado) VALUES (" + value + ")";
            mda.conMasc(SQL);
        }
        //nombre,fecha_ingreso,fecha_nacimiento,genero,type,raza,color,peso,ced_duenno,nom_duenno,ape_duenno,direccion,telefono,email,observaciones,estado
        public void updateMasco(Mascota u, int Id)
        {
            string SQL = "UPDATE UTN.mascotas SET telefono = '" + u.telefono + "', nombre = '" + u.nombre_mascota + "', fecha_ingreso = '" + u.fecha_ingreso + "', fecha_nacimiento = '" + u.fecha_nacimiento + "', genero = '" + u.sexo + "', type = '" + u.tipo_mascota + "', raza = '" + u.raza + "', color = '" + u.color + "', peso = '" + u.peso + "', direccion = '" + u.direccion + "',correo = '" + u.email + "', observaciones = '" + u.observaciones + "' ,estado = '" + u.estado + "' WHERE Id = '" + Id + "'";
            mda.conMasc(SQL);
        }
        public void deleteMascota(int Id)
        {

            string SQL = "DELETE FROM UTN.mascotas WHERE id = '" + Id + "'";
            mda.conMasc(SQL);
        }
    }
}

