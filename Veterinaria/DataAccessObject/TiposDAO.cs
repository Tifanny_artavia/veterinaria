﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessObject;
using Entity;
using System.Data;
using DataAccess;

namespace DataAccessObject
{
    public class TiposDAO
    {
        TiposDA vda = new TiposDA();

        public LinkedList<Tipo> getTipoLink()
        {
            LinkedList<Tipo> vc = new LinkedList<Tipo>();
            DataTable dataTipo = vda.selectTipo();
            DataTableReader reader = dataTipo.CreateDataReader();
            do
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Tipo vac = new Tipo(int.Parse(reader[0].ToString()), reader[1].ToString(), reader[2].ToString());
                        vc.AddLast(vac);
                    }
                }
            } while (reader.NextResult());
            return vc;
        }


        public DataTable getTipoDataTable()
        {
            return vda.selectTipo();
        }
        public LinkedList<Tipo> getTipLink()
        {
            LinkedList<Tipo> v = new LinkedList<Tipo>();
            DataTable dataTipo = vda.selectTipo();
            DataTableReader reader = dataTipo.CreateDataReader();
            do
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Tipo vac = new Tipo(int.Parse(reader[0].ToString()), reader[1].ToString(), reader[2].ToString());
                        v.AddLast(vac);
                    }
                }
            } while (reader.NextResult());
            return v;
        }



        public void addTipo(Tipo t)
        {
            string value = "'" + t.codigo + "','" + t.tipo +  "'";
            string SQL = "INSERT INTO UTN.tipo (codigo, nombre) VALUES (" + value + ")";
            vda.conTipo(SQL);
        }


        public void deleteTipo(string nombre)
        {
            string SQL = "DELETE FROM UTN.tipo WHERE nombre = '" + nombre + "'";
            vda.conTipo(SQL);
        }


        public void updateTipo(Tipo t, string tipo)
        {
            string SQL = "UPDATE UTN.tipo SET codigo = '" + t.codigo + "', tipo = '" + t.tipo + "'";
            vda.conTipo(SQL);
        }

    }
}
