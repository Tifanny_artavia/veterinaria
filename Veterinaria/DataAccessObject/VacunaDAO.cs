﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using Entity;
using System.Data;


namespace DataAccessObject
{
    public class VacunaDAO
    {
        VacunaDA vda = new VacunaDA();

        public LinkedList<Vacuna> getVacLink()
        {
            LinkedList<Vacuna> vc = new LinkedList<Vacuna>();
            DataTable dataVac = vda.SelectVacuna();
            DataTableReader reader = dataVac.CreateDataReader();
            do
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Vacuna vac = new Vacuna(int.Parse(reader[0].ToString()), reader[1].ToString(), reader[2].ToString(), reader[2].ToString(), int.Parse(reader[3].ToString()));
                        vc.AddLast(vac);
                    }
                }
            } while (reader.NextResult());
            return vc;
        }


        public DataTable getVacunaDataTable()
        {
            return vda.SelectVacuna();
        }
        public LinkedList<Vacuna> getVacunaLink()
        {
            LinkedList<Vacuna> v = new LinkedList<Vacuna>();
            DataTable dataVac = vda.SelectVacuna();
            DataTableReader reader = dataVac.CreateDataReader();
            do
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Vacuna vac = new Vacuna(int.Parse(reader[0].ToString()), reader[1].ToString(), reader[2].ToString(), reader[2].ToString(), int.Parse(reader[4].ToString()));
                        v.AddLast(vac);
                    }
                }
            } while (reader.NextResult());
            return v;
        }



        public void addVacuna(Vacuna t)
        {
            string value = "'" + t.codigo + "','" + t.nombre + "','" + t.descipcion + "','" + t.precio +  "'";
            string SQL = "INSERT INTO UTN.vacunas (codigo, nombre, descripcion, precio) VALUES (" + value + ")";
            vda.conVacuna(SQL);
        }


        public void deleteVacuna(int precio)
        {
            string SQL = "DELETE FROM UTN.vacunas WHERE precio = '" + precio + "'";
            vda.conVacuna(SQL);
        }


        public void updateVacuna(Vacuna t, int precio)
        {
            string SQL = "UPDATE UTN.vacunas SET codigo = '" + t.codigo + "', nombre = '" + t.nombre + "', descripcion = '" + t.descipcion + "', precio = '" + t.precio + "' WHERE precio = '" + precio + "'";
            vda.conVacuna(SQL);
        }

    }
}
