﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessObject;
using Entity;
using System.Data;
using DataAccess;

namespace DataAccessObject
{
    public class TratamientoDAO
    {
       TratamientoDA tda = new TratamientoDA();

        public LinkedList<Tratamiento> getTratLink()
        {
            LinkedList<Tratamiento> trt = new LinkedList<Tratamiento>();
            DataTable dataTrat = tda.selectTrat();
            DataTableReader reader = dataTrat.CreateDataReader();
            do
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Tratamiento trat = new Tratamiento(int.Parse(reader[0].ToString()), reader[1].ToString(), reader[2].ToString(), reader[2].ToString(), int.Parse(reader[4].ToString()));
                        trt.AddLast(trat);
                    }
                }
            } while (reader.NextResult());
            return trt;
        }


        public DataTable getTratDataTable()
        {
            return tda.selectTrat();
        }
        public LinkedList<Tratamiento> getTrataLink()
        {
            LinkedList<Tratamiento> t = new LinkedList<Tratamiento>();
            DataTable dataServ = tda.selectTrat();
            DataTableReader reader = dataServ.CreateDataReader();
            do
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Tratamiento tra = new Tratamiento(int.Parse(reader[0].ToString()), reader[1].ToString(), reader[2].ToString(), reader[2].ToString(), int.Parse(reader[4].ToString()));
                        t.AddLast(tra);
                    }
                }
            } while (reader.NextResult());
            return t;
        }



        public void addTrat(Tratamiento t)
        {
            string value = "'" + t.codigo + "','" + t.nombre + "','" + t.descipcion + "','" + t.precio + "'";
            string SQL = "INSERT INTO UTN.tratamientos (codigo, nombre, descripcion, precio) VALUES (" + value + ")";
            tda.conTrat(SQL);
        }


        public void deleteTrat(int precio)
        {
            string SQL = "DELETE FROM UTN.tratamientos WHERE precio = '" + precio + "'";
            tda.conTrat(SQL);
        }


        public void updateTrat(Tratamiento t, int precio)
        {
            string SQL = "UPDATE UTN.tratemientos SET codigo = '" + t.codigo + "', nombre = '" + t.nombre + "', descripcion = '" + t.descipcion + "', precio = '" + t.precio + "' WHERE precio = '" + precio + "'";
            tda.conTrat(SQL);
        }

    }
}
