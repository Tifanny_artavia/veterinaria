﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public abstract class Persona
    {
        private int _cedula;
        private string _nombre;
        private string _apellido;

        protected Persona()
        {
        }

        public Persona(int cedula, string nombre, string apellido)
        {
            this._cedula = cedula;
            this._nombre = nombre;
            this._apellido = apellido;
        }
        public Persona(string nombre)
        {
            this._nombre = nombre;
        }

        public int cedula
        {
            get { return _cedula; }
            set { _cedula = value; }
        }
        public string nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        public string apellido
        {
            get { return _apellido; }
            set { _apellido = value; }
        }
    }
}
