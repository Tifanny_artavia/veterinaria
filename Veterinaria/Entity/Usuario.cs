﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Usuario : Persona
    {
        private int _Id;
        private string _telefono;
        private string _genero;
        private string _correo;
        private string _estado;
        private string _type;
        private string _contra;

        public Usuario() : base()
        {

        }


        public Usuario(int id, int cedula, string nombre, string apellido,  string telefono, string genero, string correo, string estado,string type, string contra)
            : base(cedula, nombre, apellido)
        {
            _Id = id;
            _telefono = telefono;
            _genero = genero;
            _correo = correo;
            _estado = estado;
            _type = type;
            _contra = contra;
        }

        public Usuario(int cedula, string nombre, string apellido,string telefono, string genero, string correo, string estado, string type, string contra)
             : base(cedula, nombre, apellido)
        {

            _telefono = telefono;
            _genero = genero;
            _correo = correo;
            _estado = estado;
            _type = type;
            _contra = contra;
        }

        public int id
        {
            get { return _Id; }
            set { _Id = value; }
        }
        

        public string telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }
        public string genero
        {
            get { return _genero; }
            set { _genero = value; }
        }
        public string correo
        {
            get { return _correo; }
            set { _correo = value; }
        }
        public string estado
        {
            get { return _estado; }
            set { _estado = value; }
        }

        public string type
        {
            get { return _type; }
            set { _type = value; }
        }
        public string contra
        {
           get { return _contra; }
           set { _contra = value; }
        }

    }
}

