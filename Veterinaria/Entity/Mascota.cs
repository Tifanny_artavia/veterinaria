﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Mascota
    {
        private int _id;
        private string _nombre_mascota;
        private DateTime _fecha_ingreso;
        private DateTime _fecha_nacimiento;
        private string _sexo;
        private string _tipo_mascota;
        private string _raza;
        private string _color;
        private string _peso;
        private string _direccion;
        private string _telefono;
        private string _email;
        private string _observaciones;
        private string _estado;
        private int _cantVacuna;


        public Mascota()
        {

        }

        public Mascota(string nombre_mascota, DateTime fecha_ingreso, DateTime fecha_nacimiento, string sexo, string tipo_mascota, string raza, string color, string peso, string direccion, string telefono, string email, string observaciones, string estado)
        {
            this._nombre_mascota = nombre_mascota;
            this._fecha_ingreso = fecha_ingreso;
            this._fecha_nacimiento = fecha_nacimiento;
            this._sexo = sexo;
            this._tipo_mascota = tipo_mascota;
            this._raza = raza;
            this._color = color;
            this._peso = peso;
            this._direccion = direccion;
            this._telefono = telefono;
            this._email = email;
            this._observaciones = observaciones;
            this._estado = estado;
        }



        public Mascota(int id, string nombre_mascota, DateTime fecha_ingreso, DateTime fecha_nacimiento, string sexo, string tipo_mascota, string raza, string color, string peso, string direccion, string telefono, string email, string observaciones, string estado)
        {
            this._id = id;
            this._nombre_mascota = nombre_mascota;
            this._fecha_ingreso = fecha_ingreso;
            this._fecha_nacimiento = fecha_nacimiento;
            this._sexo = sexo;
            this._tipo_mascota = tipo_mascota;
            this._raza = raza;
            this._color = color;
            this._peso = peso;
            this._direccion = direccion;
            this._telefono = telefono;
            this._email = email;
            this._observaciones = observaciones;
            this._estado = estado;
        }
        public Mascota(int cantVacuna)
        {

            this._cantVacuna = cantVacuna;
        }

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string nombre_mascota
        {
            get { return _nombre_mascota; }
            set { _nombre_mascota = value; }
        }
        public DateTime fecha_ingreso
        {
            get { return _fecha_ingreso; }
            set { _fecha_ingreso = value; }
        }
        public DateTime fecha_nacimiento
        {
            get { return _fecha_nacimiento; }
            set { _fecha_nacimiento = value; }
        }
        public string sexo
        {
            get { return _sexo; }
            set { _sexo = value; }
        }
        public string tipo_mascota
        {
            get { return _tipo_mascota; }
            set { _tipo_mascota = value; }
        }
        public string raza
        {
            get { return _raza; }
            set { _raza = value; }
        }
        public string color
        {
            get { return _color; }
            set { _color = value; }
        }
        public string peso
        {
            get { return _peso; }
            set { _peso = value; }
        }
      
        public string direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }
        public string telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }
        public string email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string observaciones
        {
            get { return _observaciones; }
            set { _observaciones = value; }
        }
        public string estado
        {
            get { return _estado; }
            set { _estado = value; }
        }
        public int cantVacuna
        {
            get { return _cantVacuna; }
            set { _cantVacuna = value; }
        }

    }
}
