﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Tipo
    {
        int _Id;
        string _codigo;
        string _tipo;

        public Tipo(int id, string codigo, string tipo)
        {
            _Id = id;
            this._codigo = codigo;
            this._tipo = tipo;
        }

        public Tipo(string codigo, string tipo)
        {
            _codigo = codigo;
            _tipo = tipo;
        }
        public int id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        public string codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }
        public string tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }
    }
}
