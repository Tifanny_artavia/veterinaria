﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Servicio
    {
        private int _id;
        private string _codigo;
        private string _nombre;
        private string _descripcion;
        private int _precio;

        public Servicio(int id, string codigo, string nombre, string descripcion, int precio)
        {
            _id = id;
            _codigo = codigo;
            _nombre = nombre;
            _descripcion = descripcion;
            _precio = precio;
        }

        public Servicio( string codigo, string nombre, string descripcion, int precio)
        {
            _codigo = codigo;
            _nombre = nombre;
            _descripcion = descripcion;
            _precio = precio;
        }

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }


        public string codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }

        public string nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        public string descipcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }
        public int precio
        {
            get { return _precio; }
            set { _precio = value; }
        } 
    }
}
