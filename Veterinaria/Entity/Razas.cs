﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Razas
    {
        int _Id;
        string _codigo;
        string _raza;

        public Razas(int id, string codigo, string raza)
        {
            _Id = id;
            this._codigo = codigo;
            this._raza = raza;
        }
        public Razas( string codigo, string raza)
        {
            _codigo = codigo;
            _raza = raza;
        }


        public int id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        public string codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }
        public string raza
        {
            get { return _raza; }
            set { _raza = value; }
        }
    }
}

