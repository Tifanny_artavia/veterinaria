﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using Entity;
using Veterinaria.Administrador;

namespace Veterinaria
{
    public partial class Form1 : Form
    {
        UsuarioMascotaBO ubo = new UsuarioMascotaBO();
        public Form1()
        {
            InitializeComponent();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            FrmRegistrar r = new FrmRegistrar();
            r.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private bool initSesion(KeyEventArgs e)
        {
            bool init = false;
            if (txtCedula.Text.Equals("") || txtPass.Text.Equals(""))
            {
                init = false;
            }
            else
            {
                init = true;
            }
            return init;
        }
        private void loginComprobation(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (initSesion(e) == true)
                {
                    Usuario logged = ubo.getLogin(int.Parse(txtCedula.Text), txtPass.Text);
                    if (logged != null)
                    {
                        txtCedula.Text = "";
                        txtPass.Text = "";
                        if (logged.type.Equals("Administrador"))
                        {
                                LobbyAdmin admin = new LobbyAdmin(logged);
                                admin.Show();
                        }
                        else if (logged.type.Equals("Vendedor"))
                        {
                            //LobbySales ls = new LobbySales(logged);
                            //ls.Show();
                        }
                        else if (logged.type.Equals("Cajero"))
                        {
                            //LobbyCashier lc = new LobbyCashier(logged);
                            //lc.Show();
                        }

                    }
                    else
                    {
                        MessageBox.Show("Su usuario o contraseña es incorrecta");
                    }

                }
                else
                {
                    MessageBox.Show("Datos incompletos");
                }
            }
        }

        private void txtCedula_KeyDown(object sender, KeyEventArgs e)
        {
            loginComprobation(e);

        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void txtCedula_KeyDown_1(object sender, KeyEventArgs e)
        {

        }

        private void txtPass_KeyDown(object sender, KeyEventArgs e)
        {
            loginComprobation(e);

        }
    }
}
