﻿using System;
using Entity;
using Veterinaria.Administrador;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Componente.Componente2;

namespace Veterinaria.Administrador
{
    public partial class LobbyAdmin : Form
    {
        Usuario admin;
        public LobbyAdmin(Usuario admin)
        {
            InitializeComponent();
            this.admin = admin;

        }

        private void LobbyAdmin_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            FrmUsuarios cliente= new FrmUsuarios();
            cliente.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmSistema sist = new FrmSistema();
            sist.Show();
        }

        private void btnAnimal_Click(object sender, EventArgs e)
        {
            FrmMascotas mascota = new FrmMascotas();
            mascota.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FrmClientes c = new FrmClientes();
            c.Show();
        }

        private void btnServicios_Click(object sender, EventArgs e)
        {
            FrmServicios s = new FrmServicios();
            s.Show();
        }

        private void btnTrat_Click(object sender, EventArgs e)
        {
            FrmTratamiento t = new FrmTratamiento();
            t.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmVacuna v = new FrmVacuna();
            v.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FrmCom componente = new FrmCom();
            componente.Show();
        }

        private void btnPagos_Click(object sender, EventArgs e)
        {
            Button boton = new Button();
            boton.Text = "Salir";
            boton.Location = new Point(600, 200);
            boton.BackColor = Color.Gray;
            boton.Name = "btnSalir";
            boton.Click += btn_Salir_Click;
            this.Controls.Add(boton);

        }

        private void btn_Salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
