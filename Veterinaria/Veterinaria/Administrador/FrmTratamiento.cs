﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using Entity;

namespace Veterinaria.Administrador
{
    public partial class FrmTratamiento : Form
    {
        TratamientoBO tbo = new TratamientoBO();
        
        public FrmTratamiento()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AddTratamiento tadd = new AddTratamiento();
            tadd.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count < 0 && dgvUsers.CurrentCell.RowIndex != 1)
            {
                MessageBox.Show("Seleccione un tratamiento");
            }
            else
            {
                EditarTratamiento uu = new EditarTratamiento(tbo.getLinkedTrat(), Convert.ToInt32(dgvUsers.CurrentRow.Cells[4].Value));
                uu.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count < 0 && dgvUsers.CurrentCell.RowIndex != 1)
            {
                MessageBox.Show("Seleccione un servicio");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Desea eliminar  '" + dgvUsers.CurrentRow.Cells[2].Value, "Eliminar ", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    tbo.delete(Convert.ToInt32(dgvUsers.CurrentRow.Cells[4].Value));
                }
            }
        }

        private void FrmTratamiento_Load(object sender, EventArgs e)
        {
            dgvUsers.EnableHeadersVisualStyles = false;
            dgvUsers.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(40, 40, 40);
            dgvUsers.DataSource = tbo.getTraDataTable();
        }

        private void dgvUsers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
