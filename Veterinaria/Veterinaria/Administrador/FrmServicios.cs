﻿
using System.Drawing;
using System.Windows.Forms;
using BussinessObject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Veterinaria.Administrador
{
    public partial class FrmServicios : Form
    {
        ServicioBO sbo = new ServicioBO();
        public FrmServicios()
        {
            InitializeComponent();
        }

        private void FrmServicios_Load(object sender, EventArgs e)
        {
            dgvUsers.EnableHeadersVisualStyles = false;
            dgvUsers.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(40, 40, 40);
            dgvUsers.DataSource = sbo.getServDataTable();
            


        }

        private void button2_Click(object sender, EventArgs e)
        {
            AddServicio ase = new AddServicio();
            ase.Show();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count < 0 && dgvUsers.CurrentCell.RowIndex != 1)
            {
                MessageBox.Show("Seleccione una cedula");
            }
            else
            {
                EditarServicio uu = new EditarServicio(sbo.getLinkedServ(), Convert.ToInt32(dgvUsers.CurrentRow.Cells[4].Value));
                uu.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count < 0 && dgvUsers.CurrentCell.RowIndex != 1)
            {
                MessageBox.Show("Seleccione un servicio");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Desea eliminar  '" + dgvUsers.CurrentRow.Cells[2].Value, "Eliminar ", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    sbo.delete(Convert.ToInt32(dgvUsers.CurrentRow.Cells[4].Value));
                }
            }
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            

        }
    }
}
