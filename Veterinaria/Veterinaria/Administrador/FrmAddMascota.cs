﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using Entity;



namespace Veterinaria.Administrador
{
    public partial class FrmAddMascota : Form
    {
        MascotaBO mbo = new MascotaBO();
        UsuarioMascotaBO ubo = new UsuarioMascotaBO();
        LinkedList<Usuario> clients = new LinkedList<Usuario>();

        public FrmAddMascota()
        {
            InitializeComponent();
        }

        private void FrmAddMascota_Load(object sender, EventArgs e)
        {
            foreach (Usuario user in ubo.getLinkedUser())
            {
                if (user.type.Equals("Cliente"))
                {
                    clients.AddLast(user);
                    cmbDuennos.Items.Add(user.cedula + " - " + user.nombre + " - " + user.apellido);
                }
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNombre.Text) || string.IsNullOrEmpty(txtFecha_ing.Text) || string.IsNullOrEmpty(txtFecha_nac.Text) || string.IsNullOrEmpty(txtRaza.Text) || string.IsNullOrEmpty(txtColor.Text) || string.IsNullOrEmpty(txtPeso.Text) || string.IsNullOrEmpty(txtDireccion.Text) || string.IsNullOrEmpty(txtTelefono.Text) || string.IsNullOrEmpty(txtEmail.Text) || string.IsNullOrEmpty(txtObservaciones.Text))
            {
                MessageBox.Show("Error: información incompleta");
            }
            else
            {
                ubo.create(txtNombre.Text, DateTime.Parse(txtFecha_ing.Text), DateTime.Parse(txtFecha_nac.Text), cmbSexo.Text, cmbTipo.Text, txtRaza.Text, txtColor.Text, txtPeso.Text, txtDireccion.Text, txtTelefono.Text, txtEmail.Text, txtObservaciones.Text,cmbEstado.Text);
                MessageBox.Show("¡Agregado con exito!");
            }
        }
        //string nombre_mascota, DateTime fecha_ingreso, DateTime fecha_nacimiento, string sexo, string tipo_mascota, string raza, string color, string peso,string dirección, string telefono, string email, string observaciones, string estado

        private void cmbSexo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
