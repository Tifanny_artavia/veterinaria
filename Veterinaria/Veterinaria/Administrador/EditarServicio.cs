﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using Entity;

namespace Veterinaria.Administrador
{
    public partial class EditarServicio : Form
    {
        ServicioBO sbo = new ServicioBO();
        Servicio servOld;
        LinkedList<Servicio> dataServ;
        int serv;

        public EditarServicio(LinkedList<Servicio> dataServ, int selection)
        {
            InitializeComponent();
            this.dataServ = dataServ;
            this.serv = selection;
        }

        private void EditarServicio_Load(object sender, EventArgs e)
        {
            foreach (Servicio u in dataServ)
            {
                if (u.precio == serv)
                {
                    if (u.nombre.Equals("Corte"))
                    {
                        cmbNombre.SelectedIndex = 0;
                    }
                    else if (u.nombre.Equals("Baño"))
                    {
                        cmbNombre.SelectedIndex = 1;
                    }
                    else 
                    {
                        cmbNombre.SelectedIndex = 2;
                    }
                    txtDesc.Text = u.descipcion;
                    txtPrecio.Text = "" + (u.precio);
                }
            }
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDesc.Text) || string.IsNullOrEmpty(txtPrecio.Text))

            {
                MessageBox.Show("Error: información incompleta");
            }
            else
            {
                sbo.update(lblCodigo.Text, cmbNombre.Text, txtDesc.Text, int.Parse(txtPrecio.Text), serv);
                MessageBox.Show("¡Actualizado con exito!");
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    
}
