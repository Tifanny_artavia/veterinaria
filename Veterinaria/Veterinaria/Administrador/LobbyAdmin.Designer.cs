﻿
namespace Veterinaria.Administrador
{
    partial class LobbyAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSistema = new System.Windows.Forms.Button();
            this.btnPagos = new System.Windows.Forms.Button();
            this.btnAnimal = new System.Windows.Forms.Button();
            this.btnTrat = new System.Windows.Forms.Button();
            this.btnServicios = new System.Windows.Forms.Button();
            this.btnClientes = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1052, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 18);
            this.label6.TabIndex = 11;
            this.label6.Text = "x";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1023, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "-";
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = global::Veterinaria.Properties.Resources.syringe_50px;
            this.button2.Location = new System.Drawing.Point(487, 28);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(101, 90);
            this.button2.TabIndex = 14;
            this.button2.Text = "VACUNAS";
            this.button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = global::Veterinaria.Properties.Resources.staff_64px1;
            this.button1.Location = new System.Drawing.Point(133, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 90);
            this.button1.TabIndex = 13;
            this.button1.Text = "CLIENTES";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnSistema
            // 
            this.btnSistema.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSistema.Image = global::Veterinaria.Properties.Resources.services_60px;
            this.btnSistema.Location = new System.Drawing.Point(839, 28);
            this.btnSistema.Name = "btnSistema";
            this.btnSistema.Size = new System.Drawing.Size(101, 90);
            this.btnSistema.TabIndex = 12;
            this.btnSistema.Text = "SISTEMA";
            this.btnSistema.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSistema.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSistema.UseVisualStyleBackColor = true;
            this.btnSistema.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnPagos
            // 
            this.btnPagos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPagos.Image = global::Veterinaria.Properties.Resources.coin_in_hand_60px;
            this.btnPagos.Location = new System.Drawing.Point(718, 28);
            this.btnPagos.Name = "btnPagos";
            this.btnPagos.Size = new System.Drawing.Size(101, 90);
            this.btnPagos.TabIndex = 9;
            this.btnPagos.Text = "PAGOS";
            this.btnPagos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPagos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnPagos.UseVisualStyleBackColor = true;
            this.btnPagos.Click += new System.EventHandler(this.btnPagos_Click);
            // 
            // btnAnimal
            // 
            this.btnAnimal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnimal.Image = global::Veterinaria.Properties.Resources.animal_shelter_48px;
            this.btnAnimal.Location = new System.Drawing.Point(601, 28);
            this.btnAnimal.Name = "btnAnimal";
            this.btnAnimal.Size = new System.Drawing.Size(101, 90);
            this.btnAnimal.TabIndex = 7;
            this.btnAnimal.Text = "MASCOTAS";
            this.btnAnimal.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAnimal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnAnimal.UseVisualStyleBackColor = true;
            this.btnAnimal.Click += new System.EventHandler(this.btnAnimal_Click);
            // 
            // btnTrat
            // 
            this.btnTrat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTrat.Image = global::Veterinaria.Properties.Resources.pill_48px;
            this.btnTrat.Location = new System.Drawing.Point(371, 28);
            this.btnTrat.Name = "btnTrat";
            this.btnTrat.Size = new System.Drawing.Size(101, 90);
            this.btnTrat.TabIndex = 6;
            this.btnTrat.Text = "TRATAMIENTO";
            this.btnTrat.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTrat.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnTrat.UseVisualStyleBackColor = true;
            this.btnTrat.Click += new System.EventHandler(this.btnTrat_Click);
            // 
            // btnServicios
            // 
            this.btnServicios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnServicios.Image = global::Veterinaria.Properties.Resources.shower_48px;
            this.btnServicios.Location = new System.Drawing.Point(253, 28);
            this.btnServicios.Name = "btnServicios";
            this.btnServicios.Size = new System.Drawing.Size(101, 90);
            this.btnServicios.TabIndex = 5;
            this.btnServicios.Text = "SERVICIOS";
            this.btnServicios.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnServicios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnServicios.UseVisualStyleBackColor = true;
            this.btnServicios.Click += new System.EventHandler(this.btnServicios_Click);
            // 
            // btnClientes
            // 
            this.btnClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClientes.Image = global::Veterinaria.Properties.Resources.staff_64px1;
            this.btnClientes.Location = new System.Drawing.Point(12, 28);
            this.btnClientes.Name = "btnClientes";
            this.btnClientes.Size = new System.Drawing.Size(101, 90);
            this.btnClientes.TabIndex = 0;
            this.btnClientes.Text = "USUARIOS";
            this.btnClientes.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnClientes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnClientes.UseVisualStyleBackColor = true;
            this.btnClientes.Click += new System.EventHandler(this.btnClientes_Click);
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(966, 28);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(101, 90);
            this.button3.TabIndex = 15;
            this.button3.Text = "SALAS DE CIRUGÍA";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // LobbyAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(1079, 602);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSistema);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnPagos);
            this.Controls.Add(this.btnAnimal);
            this.Controls.Add(this.btnTrat);
            this.Controls.Add(this.btnServicios);
            this.Controls.Add(this.btnClientes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LobbyAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LobbyAdmin";
            this.Load += new System.EventHandler(this.LobbyAdmin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClientes;
        private System.Windows.Forms.Button btnServicios;
        private System.Windows.Forms.Button btnTrat;
        private System.Windows.Forms.Button btnAnimal;
        private System.Windows.Forms.Button btnPagos;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSistema;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}