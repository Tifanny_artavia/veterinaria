﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;

namespace Veterinaria.Administrador
{
    public partial class FrmMascotas : Form
    {
        MascotaBO mbo = new MascotaBO();
        public FrmMascotas()
        {
            InitializeComponent();
        }

        private void FrmMascotas_Load(object sender, EventArgs e)
        {
            dgvUsers.EnableHeadersVisualStyles = false;
            dgvUsers.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(40, 40, 40);
            dgvUsers.DataSource = mbo.getMascoDataTable();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmAddMascota ma = new FrmAddMascota();
            ma.Show();
        }

        private void dgvUsers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count < 0 && dgvUsers.CurrentCell.RowIndex != 1)
            {
                MessageBox.Show("Seleccione una mascota");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Desea eliminar  '" + dgvUsers.CurrentRow.Cells[1].Value, "Eliminar ", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    mbo.delete(Convert.ToInt32(dgvUsers.CurrentRow.Cells[0].Value));
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count < 0 && dgvUsers.CurrentCell.RowIndex != 1)
            {
                MessageBox.Show("Seleccione una mascota");
            }
            else
            {
                Editar_Mascota uu = new Editar_Mascota(mbo.getMasoLink(), Convert.ToInt32(dgvUsers.CurrentRow.Cells[4].Value));
                uu.Show();
            }
        }
    }
}
