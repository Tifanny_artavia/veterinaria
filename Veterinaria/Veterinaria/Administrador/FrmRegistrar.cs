﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using Veterinaria.Administrador;

namespace Veterinaria
{
    public partial class FrmRegistrar : Form
    {
        FrmUsuarios fc = new FrmUsuarios();
        UsuarioMascotaBO ubo = new UsuarioMascotaBO();
        public FrmRegistrar()
        {
            InitializeComponent();
        }
        //public FrmRegistrar(int id, string cedula, string nombre, string apellido, string telefono, string genero, string correo, string estado, string type, string contra)
        //{
        //    InitializeComponent();
        //    txtId.Text = id.ToString();
        //    txtCed.Text = cedula;
        //    txtNom.Text = nombre;
        //    txtApellido.Text = apellido;
        //    txtTel.Text = telefono;
        //    cmbGenero.Text = genero;
        //    cmbEstado.Text = estado;
        //    cmbTipo.Text = type;
        //    txtContra.Text = contra;
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCed.Text) || string.IsNullOrEmpty(txtNom.Text) || string.IsNullOrEmpty(txtApellido.Text) || string.IsNullOrEmpty(txtTel.Text)  || string.IsNullOrEmpty(txtCorreo.Text) || string.IsNullOrEmpty(txtContra.Text))
            {
                MessageBox.Show("Error: información incompleta");
            }
            else
            {
                ubo.create(int.Parse(txtCed.Text), txtNom.Text, txtApellido.Text, txtTel.Text, cmbGenero.Text, txtCorreo.Text, cmbEstado.Text, cmbTipo.Text, txtContra.Text);
                MessageBox.Show("¡Agregado con exito!");
            }
            
        }

        private void FrmRegistrar_Load(object sender, EventArgs e)
        {

        }
    }
}

