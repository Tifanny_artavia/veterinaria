﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;

namespace Veterinaria.Administrador
{
    public partial class FrmClientes : Form
    {
        UsuarioMascotaBO ubo = new UsuarioMascotaBO();
        public FrmClientes()
        {
            InitializeComponent();
        }

        private void FrmClientes_Load(object sender, EventArgs e)
        {
            dgvUsers.EnableHeadersVisualStyles = false;
            dgvUsers.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(40, 40, 40);
            dgvUsers.DataSource = ubo.getClientsDataTable();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count < 0 && dgvUsers.CurrentCell.RowIndex != 1)
            {
                MessageBox.Show("Seleccione una cedula");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Desea eliminar a '" + dgvUsers.CurrentRow.Cells[2].Value, "Eliminar usuario", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    ubo.delete(Convert.ToInt32(dgvUsers.CurrentRow.Cells[1].Value));
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count < 0 && dgvUsers.CurrentCell.RowIndex != 1)
            {
                MessageBox.Show("Seleccione una cedula");
            }
            else
            {
                EditarUsuarios uu = new EditarUsuarios(ubo.getLinkedUser(), Convert.ToInt32(dgvUsers.CurrentRow.Cells[1].Value));
                uu.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmRegistrar cliente = new FrmRegistrar();
            cliente.Show();
        }
    }
}
