﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using Entity;

namespace Veterinaria.Administrador
{
    public partial class EditarVacuna : Form
    {
        VacunaBO vbo = new VacunaBO();
        Vacuna vacOld;
        LinkedList<Vacuna> dataVacuna;
        int vacuna;
        public EditarVacuna(LinkedList<Vacuna> dataVacuna, int selection)
        {
            InitializeComponent();
            this.dataVacuna = dataVacuna;
            this.vacuna = selection;

        }

        private void EditarVacuna_Load(object sender, EventArgs e)
        {
            foreach (Vacuna u in dataVacuna)
            {

          
                if (u.precio == vacuna)
                {
                    if (u.nombre.Equals("Parvovirus"))
                    {
                        cmbNombre.SelectedIndex = 0;
                    }
                    else if (u.nombre.Equals("Distemper"))
                    {
                        cmbNombre.SelectedIndex = 1;
                    }
                    else
                    {
                        cmbNombre.SelectedIndex = 2;

                    }

                    txtDesc.Text = u.descipcion;
                    txtPrecio.Text = "" + (u.precio);
                }

            }
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDesc.Text) || string.IsNullOrEmpty(txtPrecio.Text))

            {
                MessageBox.Show("Error: información incompleta");
            }
            else
            {
                vbo.update(lblCodigo.Text, cmbNombre.Text, txtDesc.Text, int.Parse(txtPrecio.Text), vacuna);
                MessageBox.Show("¡Actualizado con exito!");
            }
        }
    }
}
