﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using Entity;

namespace Veterinaria.Administrador
{
    public partial class EditarTratamiento : Form
    {
        TratamientoBO tbo = new TratamientoBO();
        Tratamiento tratOld;
        LinkedList<Tratamiento> dataTrat;
        int trat;
        public EditarTratamiento(LinkedList<Tratamiento> dataTrat, int selection)
        {
            InitializeComponent();
            this.dataTrat = dataTrat;
            this.trat = selection;
        }

        private void EditarTratamiento_Load(object sender, EventArgs e)
        {
            foreach (Tratamiento u in dataTrat)
            {
                if (u.precio == trat)
                {
                    if (u.nombre.Equals("Antibiótico"))
                    {
                        cmbNombre.SelectedIndex = 0;
                    }
                    else if (u.nombre.Equals("Cirugía"))
                    {
                        cmbNombre.SelectedIndex = 1;
                    }
                   
                    txtDesc.Text = u.descipcion;
                    txtPrecio.Text = "" + (u.precio);
                }

            }
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDesc.Text) || string.IsNullOrEmpty(txtPrecio.Text))

            {
                MessageBox.Show("Error: información incompleta");
            }
            else
            {
                tbo.update(lblCodigo.Text, cmbNombre.Text, txtDesc.Text, int.Parse(txtPrecio.Text), trat);
                MessageBox.Show("¡Actualizado con exito!");
            }
        }
    }
}
