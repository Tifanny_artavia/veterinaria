﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entity;
using BussinessObject;
using ComposicionAgregacion;
using PolimorfismoInterfaz;

namespace Veterinaria.Administrador
{
    public partial class AddVacuna : Form
    {
        VacunaBO vbo = new VacunaBO();
        Perro pe = new Perro();
        Gato g = new Gato();

        public AddVacuna()
        {
            InitializeComponent();
            pe.nom_Vacuna();
            IVacuna[] m = { pe, g };
            foreach (IVacuna cont in m)
            {
                Console.WriteLine("Perro"+cont.nom_Vacuna());
                Console.WriteLine("Gato"+cont.dosis());
            }


        }
        //public static void appVac(Mascota animal)
        //{
        //    Console.WriteLine(animal.cantVacuna);
        //        animal.AplicarVacuna();
        //}

           

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDesc.Text) || string.IsNullOrEmpty(txtPrecio.Text))
            {
                MessageBox.Show("Error: información incompleta");
            }
            else
            {
                vbo.create(lblCodigo.Text, cmbNombre.Text, txtDesc.Text, int.Parse(txtPrecio.Text));
                MessageBox.Show("Agregada con éxito" );
            }
        }

        private void AddVacuna_Load(object sender, EventArgs e)
        {
            int id = 2;

            lblCodigo.Text = String.Format("VA{0:000}", id);
        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
