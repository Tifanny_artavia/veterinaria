﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;

namespace Veterinaria.Administrador
{
    public partial class AddServicio : Form
    {
        ServicioBO sbo = new ServicioBO();
        public AddServicio()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void AddServicio_Load(object sender, EventArgs e)
        {
            int id= 2;

             lblCodigo.Text = String.Format("SE{0:000}", id);
        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDesc.Text) || string.IsNullOrEmpty(txtPrecio.Text))
            {
                MessageBox.Show("Error: información incompleta");
            }
            else
            {
                sbo.create(lblCodigo.Text ,cmbNombre.Text, txtDesc.Text, int.Parse(txtPrecio.Text));
                MessageBox.Show("¡Agregado con exito!");
            }

        }
    }
}
