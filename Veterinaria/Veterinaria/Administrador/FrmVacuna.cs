﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;

namespace Veterinaria.Administrador
{
    public partial class FrmVacuna : Form
    {
        VacunaBO vbo = new VacunaBO();
        public FrmVacuna()
        {
            InitializeComponent();
        }

        private void FrmVacuna_Load(object sender, EventArgs e)
        {
            dgvUsers.EnableHeadersVisualStyles = false;
            dgvUsers.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(40, 40, 40);
            dgvUsers.DataSource = vbo.getVacunaDataTable();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AddVacuna addv = new AddVacuna();
            addv.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count < 0 && dgvUsers.CurrentCell.RowIndex != 1)
            {
                MessageBox.Show("Seleccione una vacuna");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Desea eliminar  '" + dgvUsers.CurrentRow.Cells[2].Value, "Eliminar ", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    vbo.delete(Convert.ToInt32(dgvUsers.CurrentRow.Cells[4].Value));
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count < 0 && dgvUsers.CurrentCell.RowIndex != 1)
            {
                MessageBox.Show("Seleccione una vacuna");
            }
            else
            {
                EditarVacuna uu = new EditarVacuna(vbo.getLinkedVacuna(), Convert.ToInt32(dgvUsers.CurrentRow.Cells[4].Value));
                uu.Show();
            }
        }
    }
}
