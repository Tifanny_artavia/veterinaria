﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using Entity;

namespace Veterinaria.Administrador
{

    public partial class EditarUsuarios : Form
    {
        Usuario userOld;
        LinkedList<Usuario> dataUsers;
        int user;
        UsuarioMascotaBO ubo = new UsuarioMascotaBO();

        public EditarUsuarios(LinkedList<Usuario> dataUsers, int selection)
        {
            InitializeComponent();
            this.dataUsers = dataUsers;
            this.user = selection;
        }

        private void EditarCliente_Load(object sender, EventArgs e)
        {
            foreach (Usuario u in dataUsers)
            {
                if (u.cedula == user)
                {
                    txtCed.Text = "" + (u.cedula);
                    txtNom.Text = u.nombre;
                    txtApellido.Text = u.apellido;
                    txtContra.Text = u.contra;
                    txtCorreo.Text = u.correo;
                    txtTel.Text = u.telefono;
                 if (u.genero.Equals("MASCULINO"))
                    {
                        cmbGenero.SelectedIndex = 0;
                    }
                else if (u.genero.Equals("FEMENINO"))
                    {
                        cmbGenero.SelectedIndex = 1;
                    }
                if (u.estado.Equals("ACTIVO"))
                    {
                        cmbEstado.SelectedIndex = 0;
                    }
                else if (u.genero.Equals("INACTIVO"))
                    {
                        cmbEstado.SelectedIndex = 1;
                    }
                    //Cajero Administrador Transportista Vendedor Constructor
                if (u.type.Equals("Adiministrador"))
                    {
                        cmbTipo.SelectedIndex = 0;
                    }
                else if (u.type.Equals("Cliente"))
                    {
                        cmbTipo.SelectedIndex = 1;
                    }
                }
            }
        }

        private void cmbTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCed.Text) || string.IsNullOrEmpty(txtNom.Text) || string.IsNullOrEmpty(txtApellido.Text) || string.IsNullOrEmpty(txtTel.Text) || string.IsNullOrEmpty(txtCorreo.Text) || string.IsNullOrEmpty(txtContra.Text))


            {
                MessageBox.Show("Error: información incompleta");
            }
            else
            {
                ubo.update(int.Parse(txtCed.Text), txtNom.Text, txtApellido.Text, txtTel.Text, cmbGenero.Text, txtCorreo.Text, cmbEstado.Text, cmbTipo.Text, txtContra.Text,user);
                MessageBox.Show("¡Actualizado con exito!");
            }
        }
    }
}
