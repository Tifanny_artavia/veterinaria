﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolimorfismoInterfaz
{
    public class Gato : IVacuna, IMascota
    {
        
        public int dosis()
        {
            return 20;
        }

        public string nom_Vacuna()
        {
            return "Parvovirus";
        }
        public string come()
        {
            return "El gato come";
        }
        public string duerme()
        {
            return "El gato duerme mucho";
        }
    }
}
