﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolimorfismoInterfaz
{
    public interface IMascota
    {
        string duerme();
        string come();
    }
}
