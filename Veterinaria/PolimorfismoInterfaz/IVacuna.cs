﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolimorfismoInterfaz
{
    public interface IVacuna
    {
        int dosis();
        string nom_Vacuna();
    }
}
