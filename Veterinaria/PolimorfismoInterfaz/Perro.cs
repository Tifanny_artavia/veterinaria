﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolimorfismoInterfaz
{
    public class Perro : IVacuna, IMascota
    {
        public string nom_Vacuna()
        {
            return "Vacuna Parvovirus";
        }

        public int dosis()
        {
            return 30;
        }
        public string come()
        {
            return "El perro come";
        }
        public string duerme()
        {
            return "El perro duerme";
        }
    }
}

