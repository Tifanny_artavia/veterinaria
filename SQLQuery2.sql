CREATE SCHEMA UTN;


CREATE TABLE UTN.users(
	id INT IDENTITY(1,1),
	cedula int NOT NULL PRIMARY KEY,
	nombre VARCHAR(20) NOT NULL,
	apellido VARCHAR(50) NOT NULL,
	telefono VARCHAR (20) NOT NULL,
	genero VARCHAR(20) NOT NULL,
	correo VARCHAR(50) NOT NULL,
	estado VARCHAR(12) NOT NULL,
	type VARCHAR(20) NOT NULL,
	password VARCHAR(20) NOT NULL,
	
);

SELECT * FROM Veterinaria.UTN.users
INSERT INTO UTN.users 
	(cedula,nombre,apellido,telefono,genero,correo,estado,type,password) 
VALUES
('208010868','Tifanny','Artavia Corella','84856795','FEMENINO','tifanny@gmail.com','Activo', 'Administrador','1234');
INSERT INTO UTN.users 
	(cedula,nombre,apellido,telefono,genero,correo,estado,type,password) 
VALUES
('201230456','Jose','Mora Blanco','87654567','MASCULINO','jose@gmail.com','Activo', 'Cliente','jose');


CREATE TABLE UTN.mascotas(
	id INT IDENTITY(1,1),
	nombre VARCHAR(20) NOT NULL,
	fecha_ingreso date NOT NULL,
	fecha_nacimiento date NOT NULL,
	genero VARCHAR(20) NOT NULL,
	type VARCHAR(20) NOT NULL,
    raza VARCHAR (20) NOT NULL,
	color VARCHAR(50) NOT NULL,
	peso VARCHAR(12) NOT NULL,
	nom_duenno VARCHAR(20) NOT NULL,
	direccion VARCHAR(50) NOT NULL,
	telefono VARCHAR (20) NOT NULL,
	email VARCHAR(20) NOT NULL,
	observaciones VARCHAR(50) NOT NULL,
	estado VARCHAR(12) NOT NULL,
);




INSERT INTO UTN.mascotas 
	(nombre,fecha_ingreso,fecha_nacimiento,genero,type,raza,color,peso,nom_duenno,direccion,telefono,email,observaciones,estado) 
VALUES
('Akira','11/12/2020','02/20/2020','Hembra','Perro','Golden','Café', '50','Jose' ,'La Tigra','87654567','jose@gmail.com','Mascota en excelente estado','Activo');


CREATE TABLE UTN.servicios_admin(
	id INT IDENTITY(1,1) PRIMARY KEY,
	codigo VARCHAR(30) NOT NULL UNIQUE,
	nombre VARCHAR(20) NOT NULL,
	descripcion VARCHAR(50) NOT NULL,
	precio int NOT NULL,
	);

	INSERT INTO UTN.servicios_admin 
	(codigo,nombre,descripcion,precio) 
VALUES
('SE001','Peluquería','Corte de pelo completo','2000');

CREATE TABLE UTN.tratamientos(
	id INT IDENTITY(1,1) PRIMARY KEY,
	codigo VARCHAR(30) NOT NULL UNIQUE,
	nombre VARCHAR(20) NOT NULL,
	descripcion VARCHAR(50) NOT NULL,
	precio int NOT NULL,
	);

		INSERT INTO UTN.tratamientos 
	(codigo,nombre,descripcion,precio) 
VALUES
('TR001','Cirugía','Cirugía menor','10000');

CREATE TABLE UTN.vacunas(
	id INT IDENTITY(1,1) PRIMARY KEY,
	codigo VARCHAR(30) NOT NULL UNIQUE,
	nombre VARCHAR(20) NOT NULL,
	descripcion VARCHAR(50) NOT NULL,
	precio int NOT NULL,
	);

		INSERT INTO UTN.vacunas 
	(codigo,nombre,descripcion,precio) 
VALUES
('VA001','Octovalente','Prevenir enfermedades','10000');

CREATE TABLE UTN.razas(
	id INT IDENTITY(1,1) PRIMARY KEY,
	codigo VARCHAR(30) NOT NULL UNIQUE,
	raza VARCHAR(20) NOT NULL,
	
	);

	INSERT INTO UTN.razas 
	(codigo,raza) 
VALUES
('LA','LABRADOR');